.PHONY: build run default
.DEFAULT_GOAL := default

build: 
	go build -o bin/coffee

run:
	go run coffee_calc.go

default:
	@echo coffee-calc
