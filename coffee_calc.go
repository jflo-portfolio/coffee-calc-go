package main

import (
	"flag"
	"fmt"
	"math"
	"strconv"
	"strings"
)

// calculate and display coffee/water amounts
func coffeeCalc(ratio string, coffee float64, water float64) (float64, float64, float64) {
	ratio_denominator, err := strconv.Atoi(strings.Split(ratio, ":")[1])
	if water == 0 && coffee == 0 {
		panic("Water and coffee can't both be zero!")
	}

	if coffee != 0 {
		water = coffee * float64(ratio_denominator)
	} else if water != 0 {
		coffee = water / float64(ratio_denominator)
	}

	brewed_coffee := water - (coffee * 1.995)

	if err != nil {
		fmt.Println(err)
	}

	water = math.Round(water*100) / 100
	coffee = math.Round(coffee*100) / 100
	brewed_coffee = math.Round(brewed_coffee*100) / 100
	return water, coffee, brewed_coffee
}

// program to calculate coffee/water amounts depending on ratio
func main() {
	ratioPtr := flag.String("r", "1:16", "ratio")
	waterPtr := flag.Float64("w", 500.0, "water")
	coffeePtr := flag.Float64("c", 0.0, "coffee")
	flag.Parse()

	allowed_ratios := [7]string{"1:13", "1:14", "1:15", "1:16", "1:17", "1:18", "1:19"}
	found := false
	for _, v := range allowed_ratios {
		if *ratioPtr == v {
			found = true
		}
	}
	if !found {
		fmt.Println("coffee ratio not found")
	}

	water, coffee, brewed_coffee := coffeeCalc(*ratioPtr, *coffeePtr, *waterPtr)

	fmt.Printf("%s coffee to water ratio\n", *ratioPtr)
	fmt.Printf("%.2f milliliters of water\n", water)
	fmt.Printf("%.2f grams of coffee\n", coffee)
	fmt.Printf("produces %.2f milliliters of brewed coffee\n", brewed_coffee)
}
