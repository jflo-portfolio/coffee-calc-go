package main

import (
	"fmt"
	"testing"
)

func TestCoffeeCalc(t *testing.T) {
	var tests = []struct {
		r                      string
		c, w                   float64
		want_w, want_c, want_b float64
	}{
		// change ratio
		{"1:19", 0, 500.0, 500.0, 26.32, 447.5},
		{"1:18", 0, 500.0, 500.0, 27.78, 444.58},
		{"1:17", 0, 500.0, 500.0, 29.41, 441.32},
		{"1:16", 0, 500.0, 500.0, 31.25, 437.66},
		{"1:15", 0, 500.0, 500.0, 33.33, 433.5},
		{"1:14", 0, 500.0, 500.0, 35.71, 428.75},
		{"1:13", 0, 500.0, 500.0, 38.46, 423.27},

		// change water
		{"1:16", 0, 250.0, 250, 15.63, 218.83},
		{"1:16", 0, 300.0, 300, 18.75, 262.59},
		{"1:16", 0, 400.0, 400, 25.0, 350.13},
		{"1:16", 0, 500.0, 500, 31.25, 437.66},
		{"1:16", 0, 750.0, 750, 46.88, 656.48},

		// change coffee
		{"1:16", 25, 0, 400, 25, 350.13},
		{"1:16", 30, 0, 480, 30, 420.15},
		{"1:16", 35, 0, 560, 35, 490.18},
		{"1:16", 40, 0, 640, 40, 560.2},
		{"1:16", 35, 0, 560, 35, 490.18},
		{"1:16", 50, 0, 800, 50, 700.25},

		// combo
		{"1:17", 50, 0, 850, 50, 750.25},
		{"1:17", 0, 450, 450, 26.47, 397.19},
		{"1:16", 25, 0, 400, 25, 350.13},
		{"1:16", 0, 450, 450, 28.13, 393.89},
		{"1:15", 30, 0, 450, 30, 390.15},
		{"1:15", 0, 450, 450, 30, 390.15},
	}

	for _, tt := range tests {
		testname := fmt.Sprintf("%s,%f,%f", tt.r, tt.c, tt.w)
		t.Run(testname, func(t *testing.T) {
			water, coffee, brewed_coffee := coffeeCalc(tt.r, tt.c, tt.w)
			if water != tt.want_w {
				t.Errorf("got %f, want %f", water, tt.want_w)
			}
			if coffee != tt.want_c {
				t.Errorf("got %f, want %f", coffee, tt.want_c)
			}
			if brewed_coffee != tt.want_b {
				t.Errorf("got %f, want %f", brewed_coffee, tt.want_b)
			}
		})
	}

}
